// @ts-check
const { defineConfig, createNotesQuery } = require("./.app/app-config");

module.exports = defineConfig({
  title: "snippets.zerodogg.org",
  sidebar: {
    links: [
      {
        url: "/n/license",
        label: "License",
        icon: "file-text",
        openInNewTab: false,
      },
    ],
    sections: [
      {
        label: "Notes",
        groups: [
          {
            label: "Bookmarklets",
            query: createNotesQuery({
              tags: ["bookmarklet"],
            }),
          },
          {
            label: "Browser user script",
            query: createNotesQuery({
              tags: ["user-script"],
            }),
          },
          {
            label: "One-liners",
            query: createNotesQuery({
              pattern: "^/One-liners/",
            }),
          },
        ],
      },
    ],
  },
});
