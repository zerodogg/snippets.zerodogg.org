---
title: Home
templateEngineOverride: njk, md
---
# snippets.zerodogg.org

My storage of simple code snippets. Built using [Eleventy
Notes](https://eleventy-notes.sandroroth.com/).

Tags available:
{{ collections.tags | renderAsList({ titleProp: 'label' }) | safe }}
