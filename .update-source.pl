#!/usr/bin/perl
use 5.036;
use IO::All;

my $inputFile   = shift(@ARGV);
my $includeFile = shift(@ARGV);

my @markdown       = io($inputFile)->slurp;
my $includeContent = io($includeFile)->slurp;

my $seenIncludeStatement = 0;
my $seenEndStatement     = 0;

my @out;

foreach my $line (@markdown) {
    if ($seenIncludeStatement) {
        if ( !$seenEndStatement ) {
            if ( $line =~ /^```/ ) {
                die("Saw multiple ``` end statements") if $seenEndStatement;
                $seenEndStatement = 1;
            }
            else {
                next;
            }
        }
    }
    push( @out, $line );
    if ( $line =~ /^```javascript/ ) {
        die("Saw multiple ```javascript statements") if $seenIncludeStatement;
        $seenIncludeStatement = 1;
        push( @out, $includeContent );
    }
}

die("Saw no ending statement") if !$seenEndStatement;

io($inputFile)->write( join( '', @out ) );
