---
title: Bookmarklet builder
tags: [ "shell" ]
---

This snippet will build a bookmarklet from a piece of JavaScript code. Needs
`uglifyjs` and `perl` with the `URI::Encode` module. Run this command, paste
your JS, then press `Ctrl-D`.

```sh
uglifyjs -c -m | perl -MURI::Encode -E 'my $s; while(<>) { $s .= $_ };say "javascript:".URI::Encode::uri_encode("void function(){".$s."}();");'
```
