---
title: Add padding around image
tags: [ "shell" ]
---

This will add a colour around an image, and convert any transparency to that
colour as well. This adds 300px to each side, adjust -border to change it. It
uses GraphicsMagick. Adjust the colour parameters and input/output filenames as
needed.

```sh
gm convert input.png -bordercolor '#14181c' -border 300x300 -bordercolor black -background '#14181c' -flatten output.png
```
