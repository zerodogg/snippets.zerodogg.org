/*
 * Copyright (C) Eskild Hustvedt 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// ==UserScript==
// @name          letterboxd-where
// @namespace     org.zerodogg.snippets
// @version       0.1.3
// @description   Sorts letterboxd "where to watch" so that "play" is always at the top
// @author        Eskild Hustvedt
// @license       AGPLv3 - http://www.gnu.org/licenses/agpl-3.0.txt
// @match         https://letterboxd.com/film/*
// @match         https://letterboxd.com/*/film/*
// ==/UserScript==
((document) => {
    const performSort = (sortNumber) => {
        const elements = document.querySelectorAll(".services p.service");

        if (elements.length === 0) {
            if (sortNumber > 6) {
                return;
            }
            setTimeout(() => performSort(sortNumber + 1), 500);
            return;
        }

        const container = document.querySelector(".services");

        if (elements === null) {
            return;
        }

        for (const element of elements) {
            const availabilityType = element.querySelector(".options");
            if (
                availabilityType !== null &&
                availabilityType.innerText.toLowerCase().indexOf("play") !== -1
            ) {
                container.insertBefore(element, container.firstChild);
            } else if (
                availabilityType !== null &&
                availabilityType.innerText.toLowerCase().indexOf("disc") !== -1
            ) {
                container.append(element);
            }
        }
    };
    performSort(1);
})(document);
