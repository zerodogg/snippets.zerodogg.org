/*
 * Copyright (C) Eskild Hustvedt 2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// ==UserScript==
// @name          letterboxd-norstores
// @namespace     org.zerodogg.snippets
// @version       0.2
// @description   Adds Norwegian stores to letterboxd "where to watch"
// @author        Eskild Hustvedt
// @license       AGPLv3 - http://www.gnu.org/licenses/agpl-3.0.txt
// @match         https://letterboxd.com/film/*
// @match         https://letterboxd.com/*/film/*
// ==/UserScript==
((document) => {
    const addPKToServices = (sortNumber) => {
        const elements = document.querySelectorAll(".services p.service");
        const container = document.querySelector(".services");

        if (
            elements.length === 0 ||
            container === undefined ||
            container === null
        ) {
            if (sortNumber > 6 || (container == null && sortNumber < 20)) {
                setTimeout(() => addPKToServices(sortNumber + 1), 500);
                return;
            }
        }
        if (container === null) {
            console.log(
                "letterboxd-pk: unable to find .services (sortNumber=" +
                    sortNumber +
                    ")",
            );
            return;
        }

        const name = document.querySelectorAll(
            "#featured-film-header > h1,.film-title-wrapper > a,.film-header-group .filmtitle .name",
        )[0].innerText;

        const services = [
            {
                URL:
                    "https://www.platekompaniet.no/search?query=" +
                    encodeURIComponent(name) +
                    "&amp;hierarchicalMenu%5Bcategories.level0%5D=Film+%26+TV",
                img: "https://www.platekompaniet.no/static/icons/apple-touch-icon.png",
                name: "Platekompaniet",
            },
            {
                URL:
                    "https://www.finn.no/bap/forsale/search.html?product_category=2.86.3922.102&product_category=2.86.3922.100&q=%22" +
                    encodeURIComponent(name) +
                    "%22",
                img: "https://www.finn.no/favicon-t-96x96.png",
                name: "Finn.no",
            },
        ];

        if (elements.length > 2) {
            document.querySelector(".services").classList.add("-showall");
            const expander = document.querySelector(".js-expand-services");
            if (expander !== null && expander !== undefined) {
                expander.setAttribute("style", "display:none");
            }
        }

        for (const service of services) {
            const link = document.createElement("p");
            link.setAttribute("class", "service");
            link.innerHTML =
                '<a href="' +
                service.URL +
                '" class="label track-event js-watch-amazon-label tooltip"  target="_blank" rel="nofollow noopener noreferrer"> <span class="brand"><img referrerpolicy="no-referrer" style="max-width:100%" src="' +
                service.img +
                '" alt="" /></span> <span class="title"><span class="name">' +
                service.name +
                '</span> </a> <span class="options js-film-availability-options"> <a class="link tooltip" href="' +
                service.URL +
                '" target="_blank" rel="nofollow noopener noreferrer">Disc</a> </span>';
            document.querySelector(".services").appendChild(link);
        }
    };
    setTimeout(() => {
        addPKToServices(1);
    }, 500);
})(document);
