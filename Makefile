NPM=cd .app && npm
devserver: _installDeps
	$(NPM) run start
build: _installDeps
	$(NPM) run build
	(curl 'https://raw.githubusercontent.com/ai-robots-txt/ai.robots.txt/refs/heads/main/robots.txt') >> .app/dist/robots.txt
_installDeps:
	@if [ ! -d .app/node_modules ]; then echo "$(NPM) install"; $(NPM) install;fi
	git update-index --skip-worktree .app/package-lock.json
updateIncludedSource:
	perl .update-source.pl ./Browser/letterboxd-length.md ./static/userscripts/letterboxd-length.user.js
	perl .update-source.pl ./Browser/letterboxd-where-sort.md ./static/userscripts/letterboxd-where-sort.user.js
	perl .update-source.pl ./Browser/letterboxd-norstores.md ./static/userscripts/letterboxd-norstores.user.js
	perl .update-source.pl ./Browser/humblebundle-steam-links.md ./static/userscripts/humblebundle-steam-links.user.js
prettier:
	prettier -w static/userscripts/*.js
	@make --no-print-directory updateIncludedSource
