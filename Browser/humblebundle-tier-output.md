---
title: Humble Bundle tiers
tags: bookmarklet
---

This snippet can be used to view the contents of the various tiers of a Humble
Bundle. The easiest way is to use the bookmarklet, but you can also just copy
the code and paste it into the browser console.

Bookmarklet: <a
href="javascript:void%20function()%7B(n=%3E%7Bvar%20c=n.querySelectorAll(%22.currency-symbol%22)[0].innerText;let%20l=Array.from(n.querySelectorAll(%22.js-tier-filter%22)).reverse(),s=%7B%7D,i=%7B%7D,u=[],f=()=%3E%7Bif(0!==l.length)%7Blet%20e=l.shift(),o=e.innerText;e.click(),setTimeout(()=%3E%7Bconst%20e=[];var%20r=n.querySelectorAll(%22.preset-prices%20%3E%20input:checked%22)[0].value;o+=%22%20[%22+c+r+%22]%22,u.push(o),i[o]=[];for(const%20l%20of%20n.querySelectorAll(%22.item-title%22))%7Bvar%20t=l.innerText;!0!==s[t]&&(e.push(t),s[t]=!0)%7Di[o].push(...e.sort()),f()%7D,500)%7Delse%7Blet%20e=%22%22;for(const%20r%20of%20u)%7Be+=%22#%20%22+r+%22%5Cn%22;for(const%20t%20of%20i[r])e+=t+%22%5Cn%22;e+=%22%5Cn%22%7Dalert(e)%7D%7D;f()%7D)(document);%0A%7D();">
Humble tiers</a>.

```javascript
((document) => {
  const currency = document.querySelectorAll(".currency-symbol")[0].innerText;
  let tiers = Array.from(
    document.querySelectorAll(".js-tier-filter")
  ).reverse();
  let seen = {};
  let tierContent = {};
  let tierOrder = [];
  let next = () => {
    if (tiers.length === 0) {
      let content = "";
      for (const t of tierOrder) {
        content += "# " + t + "\n";
        for (const entry of tierContent[t]) {
          content += entry + "\n";
        }
        content += "\n";
      }
      alert(content);
      return;
    }
    let tier = tiers.shift();
    let tierName = tier.innerText;
    tier.click();
    setTimeout(() => {
      const entries = [];
      const cost = document.querySelectorAll(
        ".preset-prices > input:checked"
      )[0].value;
      tierName += " [" + currency + cost + "]";
      tierOrder.push(tierName);
      tierContent[tierName] = [];
      for (const q of document.querySelectorAll(".item-title")) {
        let name = q.innerText;
        if (seen[name] !== true) {
          entries.push(name);
          seen[name] = true;
        }
      }
      tierContent[tierName].push(...entries.sort());
      next();
    }, 500);
  };
  next();
})(document);
```
