---
title: Toggle external CSS
tags: bookmarklet
---

A simple snippet/bookmarklet that will toggle external CSS on or off for the
current page.

Bookmarklet: <a
href="javascript:void%20function()%7Bfor(const%20a%20of%20document.querySelectorAll(%22link%22))a.disabled=!window.__cssBookmarklet;window.__cssBookmarklet=!window.__cssBookmarklet;%0A%7D();">
Toggle CSS</a>

```javascript
for (const css of document.querySelectorAll("link")) {
  css.disabled = !!!window.__cssBookmarklet;
}
window.__cssBookmarklet = !!!window.__cssBookmarklet;
```
