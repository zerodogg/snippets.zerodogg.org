---
title: Humble Bundle Steam links
tags: user-script
templateEngineOverride: njk, md
---

This user script adds a link to Steam for each item in a Humble Bundle. It works on
a bundle page, choice page, the page of a purchased bundle and in the store.


{% import "custom/userscript-info.njk" as userscript %}
{{ userscript.linkWithInfo("humblebundle-steam-links.user.js") }}

```javascript
/*
 * Copyright (C) Eskild Hustvedt 2025
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// ==UserScript==
// @name          humblebundle-steam-links
// @namespace     zerodogg.org
// @version       0.1
// @description   Add links to Steam to Humble Bundle pages
// @author        Eskild Hustvedt
// @license       AGPLv3 - http://www.gnu.org/licenses/agpl-3.0.txt
// @match         https://www.humblebundle.com/*
// ==/UserScript==
((document) => {
    const addSteamLinks = () => {
        const element = document.querySelectorAll(
            ".item-title, .key-container .key-list .key-redeemer .heading-text h4, .content-choice-title, .product-header-view .human_name-view, .entities-list .entity-title",
        );
        for (const item of element) {
            item.style.overflow = "visible";
            const steamLink = document.createElement("a");
            steamLink.setAttribute("target", "_blank");
            steamLink.setAttribute(
                "href",
                "https://duckduckgo.com/?q=!+steam+" +
                    encodeURIComponent(item.innerText.trim()),
            );
            steamLink.setAttribute("rel", "noreferrer");
            steamLink.style.color = "inherit";
            steamLink.style.fontStyle = "italic";
            steamLink.style.display = "block";
            steamLink.innerText = "[Steam]";
            steamLink.addEventListener("click", (event) => {
                event.stopPropagation();
            });

            item.append(steamLink);
        }
    };
    setTimeout(addSteamLinks, 1000);
})(document);
```
