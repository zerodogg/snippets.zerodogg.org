---
title: Fanatical order page Steam links
tags: bookmarklet
---

This snippet adds a link to Steam for each item in a purchased bundle on
Fanatical. You can either use the bookmarklet, or use the snippet directly by
pasting it into the browser console.

Bookmarklet: <a
href="javascript:void%20function()%7B(e=%3E%7Bfor(const%20r%20of%20e.querySelectorAll(%22.game-name%22))%7Bvar%20t=r.innerText;const%20n=e.createElement(%22a%22);n.setAttribute(%22href%22,%22https://store.steampowered.com/search/?term=%22+encodeURIComponent(t)+%22&ignore_preferences=1%22),n.setAttribute(%22target%22,%22_blank%22),n.innerText=t,r.replaceChildren(n)%7D%7D)(document);%0A%7D();">
Fanatical steam links</a>.

```javascript
((document) => {
  for (const game of document.querySelectorAll(".game-name")) {
    const name = game.innerText;
    const link = document.createElement("a");
    link.setAttribute(
      "href",
      "https://store.steampowered.com/search/?term=" +
        encodeURIComponent(name) +
        "&ignore_preferences=1"
    );
    link.setAttribute("target", "_blank");
    link.innerText = name;
    game.replaceChildren(link);
  }
})(document);
```
