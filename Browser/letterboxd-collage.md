---
title: Letterboxd collage
tags: bookmarklet
---

Paste the snippet into the console. Use `lb.collage()` to generate the collage,
`lb.savePage()` to save the current page (for multi-page lists) then
`lb.loadPages()` to load any saved pages (use savePage on each but the last page
of the list, and then loadPages on the last page). `lb.resetPages()` will clear
saved pages.

Once you have the collage you want, resize the browser window so that it's the
size you want, and screenshot it. Firefox has a nice builtin screenshot feature
that lets you screenshot the entire page, for instance.

```javascript
(() => {
  window.lb = {
    collage() {
      const posters = document.querySelector("ul.poster-list");
      const body = document.querySelector("body");
      posters.parentNode.removeChild(posters);
      body.innerHTML = "";
      body.classList.remove('hide-films-seen');
      body.setAttribute(
        "style",
        "background-color:#14181c; background-image: none;",
      );
      posters.setAttribute(
        "style",
        "padding-right:1em; padding-left:1em;padding-top:1em;justify-content:normal !important; pointer-events:none; user-select: none;",
      );
      body.append(posters);
    },
    _getFromLS() {
      let saved = localStorage.getItem("lbCollageSaved");
      if (saved !== undefined && saved !== null) {
        saved = JSON.parse(saved);
        if (Array.isArray(saved)) {
          return saved;
        }
      }
      return [];
    },
    savePage() {
      let pages = this._getFromLS();
      const posters = document.querySelector("ul.poster-list");
      pages.push(posters.innerHTML);
      localStorage.setItem("lbCollageSaved", JSON.stringify(pages));
    },
    loadPages() {
      const posters = document.querySelector("ul.poster-list");
      let pages = this._getFromLS().join("") + posters.innerHTML;
      posters.innerHTML = pages;
    },
    resetPages() {
      localStorage.removeItem("lbCollageSaved");
    },
  };
})();
```
