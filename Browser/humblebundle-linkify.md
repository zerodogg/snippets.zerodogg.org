---
title: Humble Bundle Steam links
tags: bookmarklet
---

This snippet adds a link to Steam for each item in a Humble Bundle. It works on
a bundle page, choice page, the page of a purchased bundle and in the store.
You can either use the bookmarklet, or use the snippet directly by pasting it
into the browser console.

Bookmarklet: <a
href="javascript:void%20function()%7B$(%22.item-title,%20.key-container%20.key-list%20.key-redeemer%20.heading-text%20h4,%20.content-choice-title,%20.product-header-view%20.human_name-view,%20.entities-list%20.entity-title%22).each(function()%7Bvar%20t=$(this);t.css(%7Boverflow:%22visible%22%7D),$('%3Ca%20target=%22_blank%22%20onclick=%22event.stopPropagation()%22%20/%3E').attr(%22href%22,%22https://duckduckgo.com/?q=!+steam+%22+encodeURIComponent(t.text().trim())).attr(%22rel%22,%22noreferrer%22).attr(%22style%22,%22color:inherit;font-style:italic;display:block;%22).text(%22[Steam]%22).appendTo(t)%7D);%0A%7D();">
Humble Steam links</a>.

```javascript
$(".item-title, .key-container .key-list .key-redeemer .heading-text h4, .content-choice-title, .product-header-view .human_name-view, .entities-list .entity-title").each(
  function () {
    let $t = $(this);
    $t.css({'overflow':'visible'});
    $('<a target="_blank" onclick="event.stopPropagation()" />')
      .attr(
        "href",
        "https://duckduckgo.com/?q=!+steam+"+
          encodeURIComponent($t.text().trim())
      )
      .attr('rel','noreferrer')
      .attr("style",
      "color:inherit;font-style:italic;display:block;")
      .text("[Steam]")
      .appendTo($t);
  }
);
```
