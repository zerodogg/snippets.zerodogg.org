---
title: Steam Deck compatibility links on Steam
tags: bookmarklet
---

This snippet adds a link to ProtonDB and CheckMyDeck for the currently viewed
game on Steam. You can either use the bookmarklet, or use the snippet directly by
pasting it into the browser console.

Bookmarklet: <a
href="javascript:void%20function()%7B(e=%3E%7Bvar%20t=e.location.href.split(%22/%22)[4];const%20a=e.createElement(%22div%22);a.append(e.createElement(%22hr%22));const%20p=e.createElement(%22p%22);a.append(p),p.innerHTML='View%20on:%20%3Ca%20target=%22_blank%22%20href=%22https://checkmydeck.ofdgn.com/explore/apps/'+t+'%22%3ECheckMyDeck%3C/a%3E,%20%3Ca%20target=%22_blank%22%20href=%22https://www.protondb.com/app/'+t+'%22%3EProtonDB%3C/a%3E',e.querySelectorAll(%22.responsive_apppage_details_right%22)[1].append(p)%7D)(document);%0A%7D();">
Steam Deck compat. links</a>.

```javascript
((document) => {
    const id = document.location.href.split('/')[4];
    const container = document.createElement('div');
    container.append(document.createElement('hr'));
    const content = document.createElement('p');
    container.append(content);
    content.innerHTML = 'View on: <a target="_blank" href="https://checkmydeck.ofdgn.com/explore/apps/'+id+'">CheckMyDeck</a>, <a target="_blank" href="https://www.protondb.com/app/'+id+'">ProtonDB</a>';
    document.querySelectorAll('.responsive_apppage_details_right')[1].append(content);
})(document);
```

