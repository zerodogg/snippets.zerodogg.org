---
title: Humble Bundle diff
---

This snippet can be used in the browser console to see the diff between two
tiers on Humble Bundle. Open the bundle, select the lower of the tiers you want
to compare and paste the snippet below and press enter. Then select the upper
of the tiers and enter `compare()`.

```javascript
var it = {};
$(".item-title").each(function () {
  it[$(this).text()] = true;
});
var compare = () => {
  let no = 0;
  $(".item-title").each(function () {
    if (!it[$(this).text()]) {
      console.log($(this).text());
      no++;
    }
  });
  console.log("A difference of " + no + " items");
};
```
