---
title: Phanpy switch account
tags: bookmarklet
---

This bookmarklet switches your current [Phanpy](https://phanpy.social/)
account to the next one, looping around if needed.

Bookmarklet: <a
href="javascript:void%20function()%7B(()=%3E%7Bvar%20o=JSON.parse(localStorage.getItem(%22accounts%22));let%20t=sessionStorage.getItem(%22currentAccount%22);void%200===t&&(t=o[0].info.id);let%20e;for(const%20a%20in%20o)if(o[a].info.id===t)%7Be=a;break%7Dif(void%200===e)throw%22Unable%20to%20locate%20account%22;let%20n=parseInt(e)+1;n%3E=o.length&&(n=0),console.log(n);var%20i=o[n].info.id,i=%22https://%22+window.location.host+%22/?account=%22+i;window.location.href=i%7D)();%0A%7D();">Phanpy: switch account</a>.

```javascript
(() => {
    const accounts = JSON.parse(localStorage.getItem("accounts"));
    let current = sessionStorage.getItem("currentAccount");
    if (current === undefined) {
        current = accounts[0].info.id;
    }
    let currentID;
    for (const accNO in accounts) {
        const acc = accounts[accNO];
        if (acc.info.id === current) {
            currentID = accNO;
            break;
        }
    }
    if (currentID === undefined) {
        throw "Unable to locate account";
    }
    let nextID = parseInt(currentID) + 1;
    if (nextID >= accounts.length) {
        nextID = 0;
    }
    console.log(nextID);
    const nextAccount = accounts[nextID].info.id;
    const newURL =
        "https://" + window.location.host + "/?account=" + nextAccount;
    window.location.href = newURL;
})();
```
