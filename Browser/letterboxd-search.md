---
title: Letterboxd search bookmarklet
tags: bookmarklet
---

This bookmark attempts to find a movie you have open in the current tab (for
instance in a webstore) and search letterboxd for it.

<a href="javascript:void%20function()%7B(e=%3E%7Blet%20r=null;for(const%20l%20of[%22.header-information%20%3E%20h1%22])%7Bvar%20n=e.querySelectorAll(l);if(n&&n[0]&&n[0].innerText)%7Br=n[0].innerText;break%7D%7Dnull===r&&(r=e.querySelector(%22title%22).innerText.replace(/%5Cs+-%5Cs+.+/g,%22%22)),window.open(%22https://letterboxd.com/search/films/%22+encodeURIComponent(r.replace(/%5C([%5E%5C)]+%5C)/g,%22%22).replace(/%5Cs+/,%22%20%22).replace(/%5Cs+$/,%22%22)))%7D)(document);%0A%7D();">Search
letterboxd</a>

```javascript
((document) => {
    const queries = [
        // Platekompaniet
        '.header-information > h1'
    ];
    const cleanup = (str) => {
        return str.replace(/\([^\)]+\)/g,'').replace(/\s+/,' ').replace(/\s+$/,'');
    };
    let search = null;
    for (const query of queries)
    {
        const attempt = document.querySelectorAll(query);
        if(attempt && attempt[0] && attempt[0].innerText)
        {
            search = attempt[0].innerText;
            break;
        }
    }
    if(search === null)
    {
        search=document.querySelector('title').innerText.replace(/\s+-\s+.+/g,'');
    }
    window.open('https://letterboxd.com/search/films/'+encodeURIComponent(cleanup(search)));
})(document);
```
