---
title: Netflix my list export
---

This will export your Netflix "my list" as a CSV file, for instance for
importing into letterboxd.

Caveats: This only includes the name of the entry on your list, no year. It
also doesn't filter out TV Series. The exported data will be in the language
currently used on Netflix, so setting Netflix to English before using it will
give you better results.

This is not provided as a bookmarklet at all. Copy the JavaScript code below
and paste it into your browser's console when you're on the My List page
(https://www.netflix.com/browse/my-list).

```javascript
((document) => {
    let list = ['Title']
    for(const entry of document.querySelectorAll('.title-card a[aria-label]'))
    {
        list.push(entry.getAttribute('aria-label').replace(',','\\,'));
    }
    const a = document.createElement('a');
    a.setAttribute('download','netflix-list.csv');
    a.setAttribute('href','data:text/csv;charset=utf-8,'+encodeURIComponent(list.join("\n")));
    document.querySelector('body').append(a);
    a.click();
})(document);
```
