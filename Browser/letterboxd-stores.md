---
title: Letterboxd store bookmarklet
tags: bookmarklet
---

### Single movie

This bookmarklet will open up the current movie on letterboxd in various
stores.

<a
href="javascript:void%20function()%7B(()=%3E%7Bvar%20t=document.querySelectorAll(%22#featured-film-header%20%3E%20h1,.film-title-wrapper%20%3E%20a%22)[0].innerText;for(const%20e%20of[%22https://www.platekompaniet.no/search/?filter.Facets[topcategoryname]=BLU-RAY+%2526+4K&filter.Facets[topcategoryname]=DVD&q=%22,%22https://www.finn.no/bap/forsale/search.html?product_category=2.86.3922.100&product_category=2.86.3922.102&sort=RELEVANCE&q=%22,%22https://www.ebay.co.uk/sch/617/i.html?_from=R40&LH_TitleDesc=0&_sop=15&_nkw=%22])window.open(e+encodeURIComponent('%22'+t+'%22'))%7D)();%0A%7D();">
Search for movie</a>

Change the URLs array to modify which store(s) are searched.

```javascript
((document) => {
  const name = document.querySelectorAll(
    "#featured-film-header > h1,.film-title-wrapper > a",
  )[0].innerText;
  const URLs = [
    "https://www.platekompaniet.no/search/?filter.Facets[topcategoryname]=BLU-RAY+%26+4K&filter.Facets[topcategoryname]=DVD&q=",
    "https://www.finn.no/bap/forsale/search.html?product_category=2.86.3922.100&product_category=2.86.3922.102&sort=RELEVANCE&q=",
    "https://www.ebay.co.uk/sch/617/i.html?_from=R40&LH_TitleDesc=0&_sop=15&_nkw=",
  ];
  for (const URL of URLs) {
    window.open(URL + encodeURIComponent('"' + name + '"'));
  }
})(document);
```

### A list

Change the URL in window.open() if you want to open another store.

#### Platekompaniet

<a href="javascript:void%20function()%7B(()=%3E%7Bfor(const%20a%20of%20document.querySelectorAll(%22.poster.film-poster%22))%7Bvar%20e=Number.parseInt(a.getAttribute(%22data-film-release-year%22)),t=a.getAttribute(%22data-film-name%22),r=e-1,e=e+1;window.open(%22https://www.platekompaniet.no/search?query=%22+encodeURIComponent(t)+%22&hierarchicalMenu%255Bcategories.level0%255D=Film+%2526+TV&range%255Bproduction_date%255D%255Bmin%255D=%22+r+%22&range%255Bproduction_date%255D%255Bmax%255D=%22+e)%7D%7D)();%0A%7D();">List: Search Platekompaniet</a>

```javascript
((document) => {
  const movies = document.querySelectorAll(
    ".poster.film-poster"
  );
  for(const movie of movies)
  {
    const year = Number.parseInt(movie.getAttribute('data-film-release-year'));
    const name = movie.getAttribute('data-film-name');
    const yearRangeMinus = year-1;
    const yearRangePlus = year+1;
    window.open('https://www.platekompaniet.no/search?query='+encodeURIComponent(name)+'&hierarchicalMenu%5Bcategories.level0%5D=Film+%26+TV&range%5Bproduction_date%5D%5Bmin%5D='+yearRangeMinus+'&range%5Bproduction_date%5D%5Bmax%5D='+yearRangePlus);
  }
})(document);
```

#### Finn.no

```javascript
((document) => {
  const movies = document.querySelectorAll(
    ".poster.film-poster"
  );
  for(const movie of movies)
  {
    const name = movie.getAttribute('data-film-name');
    window.open("https://www.finn.no/bap/forsale/search.html?product_category=2.86.3922.102&product_category=2.86.3922.100&q="+encodeURIComponent('"'+name+'"'));
  }
})(document);
```
